<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// use Illuminate\Routing\Route;

// use Illuminate\Routing\Route;

Route::get('/', 'HomeController@Home' );
Route::get('/register', 'FormController@register' );
Route::post('/welcome', 'FormController@welcome' );

Route::get('/data-table', function(){
    return view('page.dataTable');
});

Route::get('/table', function(){
    return view('page.table');
});

// CRUD Cast
// Create
// menuju ke halaman cast
Route::get('/cast/create', 'CastController@create');

// menyimpan informasi dari create ke database
Route::post('/cast', 'CastController@store');

//Read data
Route::get('/cast', 'CastController@index');

// route untuk detail cast
Route::get('/cast/{cast_id}', 'CastController@show');

// Update
// Route ke halaman edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route update data berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

// Route delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');


// Route::get('/master', function (){
//     return view('layout.master');
// });
