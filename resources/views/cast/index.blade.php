
@extends('layout.master')
@section('title')
Halaman Read Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary">Tambah</a><br> <br>
<table class="table">
    <thead class="thead-dark" align="center">
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Aksi</th>

      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=> $item)
        <tr align="center">
            <th>{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>

            <form action="/cast/{{$item->id}}" method="POST">
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-primary">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
                </td>
                        
        </tr>  

        @empty
        <tr>
            <td>Data Kategori Masih Kosong</td>
        </tr>
        @endforelse

   
    </tbody>
  </table>

  @endsection