@extends('layout.master')
@section('title')
Halaman Detail Cast
@endsection

@section('content')

<h3>Nama Lengkap : {{$cast->nama}}</h4>
<h3>Umur : {{$cast->umur}} Tahun</h3>
<h3>Bio : {{$cast->bio}}</h3>

@endsection